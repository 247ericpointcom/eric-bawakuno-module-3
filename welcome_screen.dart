import 'package:flutter/material.dart';
import 'package:shopper/constants.dart';
import 'package:shopper/login_screen.dart';
import 'package:shopper/signup_screen.dart';
import 'package:shopper/dashboard.dart';
import 'package:shopper/edit_profile.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
          child: Container(
            width: double.infinity,
            height: MediaQuery.of(context).size.height,
            padding: const EdgeInsets.symmetric(horizontal: 30, vertical: 50),
            child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: const <Widget>[
                      SizedBox(height: 20,),
                      Text(
                        "Welcome",
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 30,
                            color: kTextColor ),),
                      SizedBox( height: 20 ),
                      Text("Save your money by shopping with us ",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15 ),)

                    ],),
                  // the logo image container
                  Container(
                      height: MediaQuery.of(context).size.height/2,
                      decoration: const BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage("assets/images/logo.png"))
                      )),
                  Column(
                      children: <Widget>[
                        MaterialButton(
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: () { Navigator.push(context, MaterialPageRoute(builder: (context)=> LoginPage()));},
                          // bg color
                          color: kPrimaryLightColor,
                          shape:
                          RoundedRectangleBorder( borderRadius: BorderRadius.circular(50)),
                          child: const Text(
                            "Login",
                            style: TextStyle(
                                fontWeight: FontWeight.w600,
                                color: Colors.white ),),
                        ),
                        // margin white space
                        SizedBox(height: 20),
                        MaterialButton(
                          minWidth: double.infinity,
                          height: 60,
                          onPressed: () {
                            Navigator.push(context, MaterialPageRoute(builder: (context)=> SignUpPage()));},
                          color: kPrimaryColor,
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50)
                          ),
                          child: const Text(
                              "SignUp",
                              style: TextStyle(
                                  fontWeight: FontWeight.w600,
                                  color: Colors.white)),
                        ),
                      ]),
                ]),
          ),
        )
    );
  }
}
