// @dart=2.9
import 'package:flutter/material.dart';
import 'package:shopper/signup_screen.dart';
import 'package:shopper/welcome_screen.dart';
import 'package:shopper/constants.dart';
import 'package:shopper/login_screen.dart';
import 'package:shopper/dashboard.dart';
import 'package:shopper/edit_profile.dart';
import 'package:shopper/main.dart';
void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    home: HomePage(),
  ));
}

class HomePage extends StatelessWidget {
  // const HomePage({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        // primaryColor: kPrimaryColor,
        scaffoldBackgroundColor: Colors.white,
      ),
      home: WelcomeScreen(
            ),

    );
  }
}
